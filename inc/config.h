#pragma once
#pragma warning(disable: 4251)
#pragma warning(disable: 4273)

#pragma region DLL MACRO -- DO NOT TOUCH
/**** DLL macro -- do not touch *****/
#ifdef GOLDEN_DLL
#define GOLDEN_API __declspec(dllexport)
#else
#define GOLDEN_API __declspec(dllimport)
#endif
#pragma endregion
#pragma region PARAMETER OPTIONS -- DO NOT TOUCH
/***** Parameter options -- do not touch ****/
#define WM_Windowed 1
#define WM_Fullscreen 2

#define GAPI_D3D9 1
#pragma endregion

#pragma region APPLICATION SETTINGS -- Modify as desired
/***** Application Settings -- modify as desired *****/
#define APPLICATION_NAME _T("TEST_APP")
#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768
#define WINDOW_MODE WM_Windowed
#define MAX_FPS FPS_Default
#define GRAPHICS_API GAPI_D3D9
#pragma endregion
