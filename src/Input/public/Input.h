#pragma once
#include "forward.h"

//////////////////////////////////////////////////////////////////////////////////////////////
// Description:																				//
//		This input library comes with two things:											//
//			1. Input System	(required)														//
//			2. Input Listener (optional)													//
//																							//
//		The Input class encapsulates the windows messaging and input mechanisms. Clients	//
//		can create a fully initialized Win32 input system by simply calling the static		//
//		Create function and passing the arguments from WinMain.								//
//																							//
//		There are two options for responding to input events:								//
//			1. Polling																		//
//			2. Listening (recommended)														//
//																							//
// Limits:																					//
//		* This class is a Singleton.														//
//		* If bDispatchMessages was set to true, you must call update every frame,			//
//		  regardless of whether or not you use an input listener.							//
//																							//
// Usage:																					//
//		1. Create a Params object for Input and fill out required members.					//
//		2. Create the Input instance with the Params object, and store its singleton		//
//		   instance in the object you wish to manage its lifetime with.						//
//		3A. If you prefer to manually poll for input, call Update every frame and then		//
//		   use the IsKeyDown/Up and IsMouseDown/Up functions to check for input.			//
//		3B. If you prefer to listen for input, implement the IInputListener interface and	//
//			pass an instance of it to Input via the SetListener function. When an input		//
//			event happens, the appropriate function on the listener will be called.			//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

namespace gas
{
	class GOLDEN_API IInputListener
	{
		/***** Input Events *****/
	public:
		virtual void	OnKeyPressed(const VirtualKey key) {};
		virtual void	OnKeyDown(const VirtualKey key) {};
		virtual void	OnKeyLifted(const VirtualKey key) {};

		virtual void	OnMousePressed(const MouseButton button) {};
		virtual void	OnMouseDown(const MouseButton button, const POINT& cursorClientPos) {};
		virtual void	OnMouseLifted(const MouseButton button, const POINT& cursorClientPos, const POINT& posOnDown) {}; // 3rd argument is cursor's position when it was first down, 2nd argument is the position when it was lifted.
		virtual void	OnMouseMove(const POINT& currClientPos, const POINT& lastClientPos) {};
		virtual void	OnMouseScroll(const int delta) {};

		virtual void	OnLostFocus() {};
		virtual void	OnGainedFocus() {};
		virtual void	OnExitClicked() {};
	};

	namespace Params
	{
		struct GOLDEN_API Input
		{
			// Required
			HWND		hWnd;					// Handle to the application window, from WindowManager (if using our Viewer).

			// Optional
			bool		bDispatchMessages;		// Whether or not to dispatch Windows messages for you.  If true, Input will loop through and dispatch all Windows messages when Update is called. Set it to false if you write your own message loop.  Defaults to true.

			Input()
				: hWnd(nullptr)
				, bDispatchMessages(true)
			{
			}
		};
	}

	class GOLDEN_API Input
	{
		/***** Public Interface *****/
	public:
		void			Update();	// must be called every frame if polling for input, or if bDispatchMessages was true
		void			SetListener(sptr<IInputListener> pListener);
		void			RemoveListener();
		bool			HasFocus() const;
		void			SendCloseApplicationMessage();

		bool			IsAnyKeyDown() const;
		bool			IsKeyDown(const VirtualKey key) const;
		bool			IsKeyUp(const VirtualKey key) const;

		bool			IsAnyMouseDown(const MouseButton button) const;
		bool			IsMouseDown(const MouseButton button) const;
		bool			IsMouseUp(const MouseButton button) const;
		POINT			GetMousePos() const;

		/***** Windows Messaging Procedure *****/
	public:
		static LRESULT CALLBACK WinProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		/***** Init *****/
	public:
		static uptr<Input>	Create(const Params::Input& params);

	private:
		friend struct std::default_delete<Input>;
		Input();
		~Input();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		Input(const Input&) = delete;
		Input& operator=(const Input&) = delete;
	};
}