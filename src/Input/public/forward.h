#pragma once
#include "globals.h"

namespace gas
{
	enum class VirtualKey
	{
		Zero = 0x30,
		One = 0x31,
		Two = 0x32,
		Three = 0x33,
		Four = 0x34,
		Five = 0x35,
		Six = 0x36,
		Seven = 0x37,
		Eight = 0x38,
		Nine = 0x39,
		A = 0x41,
		B = 0x42,
		C = 0x43,
		D = 0x44,
		E = 0x45,
		F = 0x46,
		G = 0x47,
		H = 0x48,
		I = 0x49,
		J = 0x4A,
		K = 0x4B,
		L = 0x4C,
		M = 0x4D,
		N = 0x4E,
		O = 0x4F,
		P = 0x50,
		Q = 0x51,
		R = 0x52,
		S = 0x53,
		T = 0x54,
		U = 0x55,
		V = 0x56,
		W = 0x57,
		X = 0x58,
		Y = 0x59,
		Z = 0x5A,
		Escape = 0x1B,
		F1 = 0x70,
		F2 = 0x71,
		F3 = 0x72,
		F4 = 0x73,
		F5 = 0x74,
		F6 = 0x75,
		F7 = 0x76,
		F8 = 0x77,
		F9 = 0x78,
		F10 = 0x79,
		F11 = 0x7A,
		F12 = 0x7B,
		GraveAccent = 0xC0,
		Minus = 0xBD,
		Equals = 0xBB,
		Backspace = 0x08,
		OpenSquareBracket = 0xDB,
		CloseSquareBracket = 0xDD,
		Backslash = 0xDC,
		Semicolon = 0xBA,
		Apostrophe = 0xDE,
		Comma = 0xBC,
		Period = 0xBE,
		ForwardSlash = 0xBF,
		Enter = 0x0D,
		Tab = 0x09,
		Capslock = 0x3A,
		LeftShift = 0xA0,
		RightShift = 0xA1,
		LeftCtrl = 0xA2,
		RightCtrl = 0xA3,
		LeftAlt = 0xA4,
		RightAlt = 0xA5,
		Space = 0x20,
		ArrowLeft = 0x25,
		ArrowRight = 0x27,
		ArrowUp = 0x26,
		ArrowDown = 0x28,
		Insert = 0x2D,
		Delete = 0x2E,
		Home = 0x24,
		End = 0x23,
		PageUp = 0x21,
		PageDown = 0x22,
		LeftSuper = 0x5B,
		RightSuper = 0x5C,
		Numpad0 = 0x60,
		Numpad1 = 0x61,
		Numpad2 = 0x62,
		Numpad3 = 0x63,
		Numpad4 = 0x64,
		Numpad5 = 0x65,
		Numpad6 = 0x66,
		Numpad7 = 0x67,
		Numpad8 = 0x68,
		Numpad9 = 0x69,
		NumpadDivide = 0x6F,
		NumpadMultiply = 0x6A,
		NumpadMinus = 0x6D,
		NumpadPlus = 0x6B,
		NumpadDot = 0x6E,
		NumpadEnter = 0x9C
	};
	enum class AsciiKey : ubyte
	{
		Zero = 0x0B,
		One = 0x02,
		Two = 0x03,
		Three = 0x04,
		Four = 0x05,
		Five = 0x06,
		Six = 0x07,
		Seven = 0x08,
		Eight = 0x09,
		Nine = 0x0A,
		A = 0x1E,
		B = 0x30,
		C = 0x2E,
		D = 0x20,
		E = 0x12,
		F = 0x21,
		G = 0x22,
		H = 0x23,
		I = 0x17,
		J = 0x24,
		K = 0x25,
		L = 0x26,
		M = 0x32,
		N = 0x31,
		O = 0x18,
		P = 0x19,
		Q = 0x10,
		R = 0x13,
		S = 0x1E,
		T = 0x14,
		U = 0x16,
		V = 0x2F,
		W = 0x11,
		X = 0x2D,
		Y = 0x15,
		Z = 0x2C,
		Escape = 0x01,
		F1 = 0x3B,
		F2 = 0x3C,
		F3 = 0x3D,
		F4 = 0x3E,
		F5 = 0x3F,
		F6 = 0x40,
		F7 = 0x41,
		F8 = 0x42,
		F9 = 0x43,
		F10 = 0x44,
		F11 = 0x57,
		F12 = 0x58,
		GraveAccent = 0x29,
		Minus = 0x0C,
		Equals = 0x0D,
		Backspace = 0x0E,
		OpenSquareBracket = 0x1A,
		CloseSquareBracket = 0x1B,
		Backslash = 0x2B,
		Semicolon = 0x27,
		Apostrophe = 0x28,
		Comma = 0x33,
		Period = 0x34,
		ForwardSlash = 0x35,
		Enter = 0x1C,
		Tab = 0x0F,
		Capslock = 0x3A,
		LeftShift = 0x2A,
		RightShift = 0x36,
		LeftCtrl = 0x1D,
		RightCtrl = 0x9D,
		LeftAlt = 0x38,
		RightAlt = 0xB8,
		Space = 0x39,
		ArrowLeft = 0xCB,
		ArrowRight = 0xCD,
		ArrowUp = 0xC8,
		ArrowDown = 0xD0,
		Insert = 0xD2,
		Delete = 0xD3,
		Home = 0xC7,
		End = 0xCF,
		PageUp = 0xC9,
		PageDown = 0xD1,
		Super = 0xDB,
		Windows = Super,
		Numpad0 = 0x52,
		Numpad1 = 0x4F,
		Numpad2 = 0x50,
		Numpad3 = 0x51,
		Numpad4 = 0x4B,
		Numpad5 = 0x4C,
		Numpad6 = 0x4D,
		Numpad7 = 0x47,
		Numpad8 = 0x48,
		Numpad9 = 0x49,
		NumpadDivide = 0xB5,
		NumpadMultiply = 0x37,
		NumpadMinus = 0x4A,
		NumpadPlus = 0x4E,
		NumpadDot = 0x53,
		NumpadEnter = 0x9C
	};
	enum class MouseButton
	{
		LeftClick = 0x01,
		RightClick = 0x02,
		WheelScroll = 0x03,
		MiddleClick = 0x04,
	};
}