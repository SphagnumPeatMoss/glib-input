#include "Input.h"
#include <bitset>

namespace gas
{
	using std::bitset;

	struct Input::Impl
	{
		static Input*	s_pInstance;

		sptr<IInputListener>	pListener;
		static const int		NUM_KEYS = 256;
		static const int		NUM_BUTTONS = 3;

		bitset<NUM_KEYS>		lastKeys;
		bitset<NUM_KEYS>		currKeys;
		bitset<NUM_BUTTONS>		lastButtons;
		bitset<NUM_BUTTONS>		currButtons;

		POINT					lastCursorClientPos;
		POINT					currCursorClientPos;
		POINT					mouseDownStartPos;

		bool					hasFocus;
		bool					isDispatcher;
		HWND					hWnd;

		Impl()
			: hasFocus(true)
			, isDispatcher(true)
			, hWnd(nullptr)
		{
			ZeroMemory(&lastCursorClientPos, sizeof(lastCursorClientPos));
			ZeroMemory(&currCursorClientPos, sizeof(currCursorClientPos));
			ZeroMemory(&mouseDownStartPos, sizeof(mouseDownStartPos));
		}

		void OnLostFocus()
		{
			hasFocus = false;
			if (pListener)
				pListener->OnLostFocus();
		}
		void OnGainedFocus()
		{
			hasFocus = true;
			if (pListener)
				pListener->OnGainedFocus();
		}
		void OnExitClicked()
		{
			if (pListener)
				pListener->OnExitClicked();
			PostQuitMessage(0);
		}

		void OnKeyDown(const WPARAM wParam)
		{
			// Update the current keyboard state
			const VirtualKey key = VirtualKey(wParam);
			currKeys[int(key)] = true;

			// Call the listener
			if (pListener)
				pListener->OnKeyDown(key);
		}
		void OnKeyLifted(const WPARAM wParam)
		{
			// Update the current keyboard state
			const VirtualKey key = VirtualKey(wParam);
			currKeys[int(key)] = false;

			// Call the listener
			if (pListener)
				pListener->OnKeyLifted(key);
		}
		void OnButtonDown(const WPARAM wParam, const LPARAM lParam)
		{
			// Update the current mouse state
			const MouseButton button = MouseButton(wParam);
			currButtons[int(button)] = true;

			currCursorClientPos.x = GET_X_LPARAM(lParam);	// LOWORD(lParam)
			currCursorClientPos.y = GET_Y_LPARAM(lParam);	// HIWORD(lParam)

			// If this button was previously up, save this position until it's lifted
			if (lastButtons[int(button)])
				mouseDownStartPos = currCursorClientPos;

			// Call the listener
			if (pListener)
				pListener->OnMouseDown(button, currCursorClientPos);
		}
		void OnButtonLifted(const WPARAM wParam, const LPARAM lParam)
		{
			// Update the current mouse state
			const MouseButton button = MouseButton(wParam);
			currButtons[int(button)] = false;

			currCursorClientPos.x = GET_X_LPARAM(lParam);	// LOWORD(lParam)
			currCursorClientPos.y = GET_Y_LPARAM(lParam);	// HIWORD(lParam)

			// Call the listener
			if (pListener)
				pListener->OnMouseLifted(button, currCursorClientPos, mouseDownStartPos);
		}
		void OnMouseMove(const WPARAM wParam, const LPARAM lParam)
		{
			// Update the current mouse state
			currCursorClientPos.x = GET_X_LPARAM(lParam);	// LOWORD(lParam)
			currCursorClientPos.y = GET_Y_LPARAM(lParam);	// HIWORD(lParam)

			// Call the listener
			if (pListener)
				pListener->OnMouseMove(currCursorClientPos, lastCursorClientPos);
		}
		void OnMouseScroll(const WPARAM wParam)
		{
			// Get the amount scrolled.  Positive == scrolled up, negative == scrolled down.
			int delta = GET_WHEEL_DELTA_WPARAM(wParam);	// don't use HIWORD -- it will be unsigned

			// Call the listener
			if (pListener)
				pListener->OnMouseScroll(delta);
		}
	};
	Input* Input::Impl::s_pInstance = nullptr;

	void Input::Update()
	{
		// Message dispatch loop
		if (m->isDispatcher)
		{
			static MSG msg;
			ZeroMemory(&msg, sizeof(msg));
			while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		// Save current state as last state, then clear current state
		m->lastKeys = m->currKeys;
		m->lastButtons = m->currButtons;
		m->lastCursorClientPos = m->currCursorClientPos;

		m->currKeys.reset();
		m->currButtons.reset();
	}
	void Input::SetListener(sptr<IInputListener> pListener)
	{
		m->pListener = pListener;
	}
	void Input::RemoveListener()
	{
		m->pListener.reset();
	}
	bool Input::HasFocus() const
	{
		return m->hasFocus;
	}
	void Input::SendCloseApplicationMessage()
	{
		BOOL result = PostMessage(m->hWnd, WM_CLOSE, 0, 0);
		assert(result && "Input::SendCloseApplicaitonMessage -- Failed to post WM_CLOSE message. Is the window handle valid?");
	}

	bool Input::IsAnyKeyDown() const
	{
		return m->currKeys.any();
	}
	bool Input::IsKeyDown(const VirtualKey key) const
	{
		return m->currKeys[int(key)];
	}
	bool Input::IsKeyUp(const VirtualKey key) const
	{
		return !m->currKeys[int(key)];
	}

	bool Input::IsAnyMouseDown(const MouseButton button) const
	{
		return m->currButtons.any();
	}
	bool Input::IsMouseDown(const MouseButton button) const
	{
		return m->currButtons[int(button)];
	}
	bool Input::IsMouseUp(const MouseButton button) const
	{
		return !m->currButtons[int(button)];
	}
	POINT Input::GetMousePos() const
	{
		return m->currCursorClientPos;
	}

	LRESULT CALLBACK Input::WinProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
			// Key/button presses
		case WM_KEYDOWN:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnKeyDown(wParam); break;
		case WM_KEYUP:			if (Impl::s_pInstance) Impl::s_pInstance->m->OnKeyLifted(wParam); break;
		case WM_MBUTTONDOWN:	if (Impl::s_pInstance) Impl::s_pInstance->m->OnButtonDown(wParam, lParam); break;
		case WM_MBUTTONUP:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnButtonLifted(wParam, lParam); break;
		case WM_MOUSEMOVE:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnMouseMove(wParam, lParam); break;
		case WM_MOUSEWHEEL:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnMouseScroll(wParam); break;

			// Focus
		case WM_SETFOCUS:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnLostFocus(); break;
		case WM_KILLFOCUS:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnGainedFocus(); break;

			// Exiting
		case WM_CLOSE:
		case WM_DESTROY:		if (Impl::s_pInstance) Impl::s_pInstance->m->OnExitClicked(); break;

		default:				return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}

		return 0;
	}

	uptr<Input> Input::Create(const Params::Input& params)
	{
		uptr<Input> pInput;

		assert(params.hWnd != nullptr && "Input::Create -- Attempted to initialize Input system with an invalid window handle.");
		if (params.hWnd)
		{
			pInput.reset(new Input());
			pInput->m->hWnd = params.hWnd;
			pInput->m->isDispatcher = params.bDispatchMessages;
		}

		return pInput;
	}
	Input::Input()
		: m(make_unique<Impl>())
	{
		assert(!Impl::s_pInstance && "Input::Create -- Only one input instance may exist.");
		Impl::s_pInstance = this;
	}
	Input::~Input()
	{
		Impl::s_pInstance = nullptr;
	}
}
