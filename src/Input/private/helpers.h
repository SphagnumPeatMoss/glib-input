#pragma once
#include "forward.h"

namespace gas
{
	VirtualKey AsciiToVk(const AsciiKey key);
	AsciiKey VkToAscii(const VirtualKey key);
}